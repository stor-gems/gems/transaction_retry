# frozen_string_literal: true

module OpenStaxTransactionRetry
  VERSION = '1.2.0'
end
